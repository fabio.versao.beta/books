<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Repositories\LivroRepositoryInterface;
use Repositories\PresenterRepositoryInterface;

class LivroController extends Controller
{
    /**
     * Livro Repository
     *
     * @var LivroRepositoryInterface
     */
    protected $livro;

    public function __construct(LivroRepositoryInterface $livro)
    {
        $this->livro = $livro;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $livro = $this->livro->todos();

        return PresenterRepositoryInterface::resultado($livro);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aleatorio($type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function velho()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function novo()
    {
        //
    }

}
