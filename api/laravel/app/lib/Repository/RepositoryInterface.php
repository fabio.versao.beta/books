<?php namespace Repositories;

interface RepositoryInterface
{
    public function todos();

    public function aleatorio($type);

    public function velho();

    public function novo();

    public function resultado($parametro);
}
